const {userModel,
  userRegisterValidator,
  userLoginValidator,
  userEmailValidator} = require('../models/user.models');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');

const JWT_SECRET = process.env.JWT_SECRET;
const saltRounds = +process.env.BCRYPT_SALT;

module.exports = {
  register: async (req, res) => {
    try {
      const userData = req.body;

      const {error} = userRegisterValidator.validate(userData);
      if (error) {
        return res.status(400).send({message: error.details[0].message});
      }
      const {email, password, role} = userData;
      const existedUser = await userModel.findOne({email});
      if (existedUser) {
        return res.status(400).send({
          message: 'This email is already registered',
        });
      }
      const encryptedPassword = await bcrypt.hash(password, saltRounds);
      await userModel.create({
        role,
        email,
        password: encryptedPassword,
      });
      res.status(200).send({message: 'Profile created successfully'});
    } catch (error) {
      res.status(500).send({message: 'Server error'});
    }
  },
  login: async (req, res) => {
    try {
      const loginData = req.body;
      const {error} = userLoginValidator.validate(loginData);
      if (error) {
        return res.status(400).send({message: error.details[0].message});
      }
      const {email, password} = loginData;
      const user = await userModel.findOne({email});
      if (!user) {
        return res.status(400).send({message: 'User not found'});
      }
      const isPasswordsMatch = await bcrypt.compare(password, user.password);
      if (!isPasswordsMatch) {
        return res.status(400).send({message: 'Wrong password'});
      }
      const token = jwt.sign({
        userId: user._id,
        role: user.role,
      }, JWT_SECRET, {expiresIn: '2h'});
      res.status(200).send({jwt_token: token});
    } catch (error) {
      res.status(500).send({message: 'Server error'});
    }
  },
  forgotPassword: async (req, res) => {
    try {
      const reqData = req.body;
      const {error} = userEmailValidator.validate(reqData);
      if (error) {
        return res.status(400).send({message: error.details[0].message});
      }
      const email = req.body.email;
      const transporter = nodemailer.createTransport({
        port: 465,
        host: 'smtp.gmail.com',
        auth: {
          user: process.env.EMAIL,
          pass: process.env.EMAIL_PASS,
        },
        secure: true,
      });
      const newPassword = Math.random()
          .toString(36).replace(/[^a-zA-Z]+/g, '')
          .split(0, 5);
      const encryptedPassword = await bcrypt.hash(newPassword[0], saltRounds);
      console.log(typeof newPassword);
      const user = await userModel.findOne({email: email});
      if (!user) {
        return res.status(200)
            .send({message: 'User with this email does\'nt exist'});
      }
      const {acknowledged} = await userModel
          .updateOne(user, {password: encryptedPassword});
      if (!acknowledged) {
        return res.status(500)
            .send({message: 'Server error'});
      }
      await transporter.sendMail({
        from: `"EPAM Lab | NodeJS HW3" <${process.env.EMAIL}>`,
        to: email,
        subject: 'Forgotten password: EPAM Lab | NodeJS HW3',
        text: `Your new password is: ${newPassword}`,
        html: `<h3>Forgotten password</h3>
        <p>Your new password is: <h3>${newPassword}</h3></p>`,
      });
      res.status(200)
          .send({message: 'New password sent to your email address'});
    } catch (error) {
      console.log(error);
      res.status(500).send({message: 'Server error'});
    }
  },
};
