const {
  loadModel,
  loadValidator,
  loadStates,
  limitLoadValidator,
} = require('../models/load.model');
const {truckModel, truckParameters} = require('../models/truck.models');

const DRIVER = 'DRIVER';
const SHIPPER = 'SHIPPER';

module.exports = {
  addLoad: async (req, res) => {
    try {
      const userId = req.userId;
      const userRole = req.role;
      if (userRole !== SHIPPER) {
        return res.status(400)
            .send({message: 'Permission allowed only for shipper'});
      }
      const reqData = req.body;
      const {error} = loadValidator.validate(reqData);
      if (error) {
        return res.status(400).send({message: error.details[0].message});
      };
      await loadModel.create({
        created_by: userId,
        name: reqData.name,
        payload: reqData.payload,
        pickup_address: reqData.pickup_address,
        delivery_address: reqData.delivery_address,
        dimensions: reqData.dimensions,
      });
      res.status(200).send({message: 'Load created successfully'});
    } catch (error) {
      res.status(500).send({message: 'Server error'});
    }
  },
  getAllLoads: async (req, res) => {
    try {
      const userId = req.userId;
      const userRole = req.role;
      const {offset = 0, limit = 10, status = ''} = req.query;
      const {error} = limitLoadValidator.validate(limit);
      if (error) {
        return res.status(400).send({message: error.details[0].message});
      }
      let loads = [];
      switch (userRole) {
        case DRIVER:
          if (status && ['ASSIGNED', 'SHIPPED'].indexOf(status) >= 0) {
            loads = await loadModel
                .find({assigned_to: userId, status})
                .skip(offset)
                .limit(limit);
          } else {
            loads = await loadModel
                .find({assigned_to: userId})
                .skip(offset)
                .limit(limit);
          }
          return res.status(200).send({loads});
        case SHIPPER:
          if (status) {
            loads = await loadModel
                .find({
                  created_by: userId,
                  status: status})
                .skip(offset)
                .limit(limit);
          } else {
            loads = await loadModel
                .find({created_by: userId})
                .skip(offset)
                .limit(limit);
          }

          return res.status(200).send({loads});
        default:
          res.status(500).send({message: 'Server error'});
          break;
      }
    } catch (error) {
      res.status(500).send({message: 'Server error'});
    }
  },
  getActiveLoad: async (req, res) => {
    try {
      const userId = req.userId;
      const userRole = req.role;
      if (userRole !== DRIVER) {
        return res.status(400)
            .send({message: 'Permission allowed only for drivers'});
      }
      const activeLoad = await loadModel
          .findOne({assigned_to: userId, status: 'ASSIGNED'}) || [];
      res.status(200).send({load: activeLoad});
    } catch (error) {
      res.status(500).send({message: 'Server error'});
    }
  },
  nextLoadState: async (req, res) => {
    try {
      const userId = req.userId;
      const userRole = req.role;
      if (userRole !== DRIVER) {
        return res.status(400)
            .send({message: 'Permission allowed only for drivers'});
      }
      const load = await loadModel
          .findOne({assigned_to: userId, status: 'ASSIGNED'});
      const currentIndex = loadStates.indexOf(load.state);
      if (currentIndex >= 0 &&
        currentIndex + 1 < loadStates.length) {
        const nextState = loadStates[currentIndex + 1];
        console.log(nextState);
        if (nextState === loadStates[loadStates.length - 1]) {
          await truckModel.updateOne({assigned_to: userId}, {status: 'IS'});
          await loadModel.updateOne({assigned_to: userId}, {status: 'SHIPPED'});
        }
        const logs = load.logs;
        logs.push({
          message: `Load state changed to '${nextState}'`,
          time: Date.now(),
        });
        await loadModel
            .updateOne({_id: load._id}, {state: nextState, logs});
        return res.status(200)
            .send({message: `Load state changed to '${nextState}'`});
      }
      res.status(400)
          .send({
            message: `Load is already delivered`,
          });
    } catch (error) {
      res.status(500).send({message: 'Server error'});
    }
  },
  getLoadById: async (req, res) => {
    try {
      const userId = req.userId;
      const userRole = req.role;
      const loadId = req.params['id'];
      let load = null;
      switch (userRole) {
        case DRIVER:
          load = await loadModel
              .findOne({_id: loadId, assigned_to: userId}) || {};
          if (!load) {
            return res.status(400).send({message: 'Load not found'});
          }
          return res.status(200).send({load});
        case SHIPPER:
          load = await loadModel
              .findOne({_id: loadId, created_by: userId});
          if (!load) {
            return res.status(400).send({message: 'Load not found'});
          }
          return res.status(200).send({load});
        default:
          return res.status(500).send({message: 'Server error 1'});
      }
    } catch (error) {
      res.status(500).send({message: 'Server error'});
    }
  },
  updateLoadbyId: async (req, res) => {
    try {
      const userId = req.userId;
      const userRole = req.role;
      const loadId = req.params['id'];
      const loadData = req.body;
      const {error} = loadValidator.validate(loadData);
      if (error) {
        return res.status(400).send({message: error.details[0].message});
      }
      if (userRole !== SHIPPER) {
        return res.status(400)
            .send({message: 'Permission allowed only for shipper'});
      }
      if (!loadId) {
        return res.status(400).send({message: 'Specify the load id'});
      }
      const load = await loadModel
          .findOne({_id: loadId, created_by: userId});
      if (load.status !== 'NEW') {
        return res.status(400).send({message: 'You can update only new loads'});
      }
      const {acknowledged} = await loadModel.updateOne(load, loadData);
      if (!acknowledged) {
        return res.status(500)
            .send({message: 'Database can\`t update the load'});
      }
      res.status(200).send({message: 'Load details changed successfully'});
    } catch (error) {
      res.status(500).send({message: 'Server error'});
    }
  },
  deleteLoadById: async (req, res) => {
    try {
      const userId = req.userId;
      const userRole = req.role;
      const loadId = req.params['id'];
      if (userRole !== SHIPPER) {
        return res.status(400)
            .send({message: 'Permission allowed only for shipper'});
      }
      if (!loadId) {
        return res.status(400).send({message: 'Specify the load id'});
      }
      const load = await loadModel.findOne({_id: loadId, created_by: userId});
      if (load.status !== 'NEW') {
        return res.status(400).send({message: 'You can delete only new loads'});
      }
      const {acknowledged} = await loadModel.deleteOne(load);
      if (!acknowledged) {
        return res.status(500)
            .send({message: 'Database can\`t delete the load'});
      }
      res.status(200).send({message: 'Load deleted successfully'});
    } catch (error) {
      res.status(500).send({message: 'Server error'});
    }
  },
  postLoad: async (req, res) => {
    try {
      const userId = req.userId;
      const userRole = req.role;
      const loadId = req.params['id'];
      if (userRole !== SHIPPER) {
        return res.status(400)
            .send({message: 'Permission allowed only for shipper'});
      }
      if (!loadId) {
        return res.status(400).send({message: 'Specify the load id'});
      }
      // find load
      const loadToPost = await loadModel
          .findOne({_id: loadId, created_by: userId, status: 'NEW'});
      if (!loadToPost) {
        return res.status(400)
            .send({message: 'This load already in work'});
      }

      // find all free trucks
      const availableTrucks = await truckModel
          .find({status: 'IS', assigned_to: {$ne: null}});
      if (!availableTrucks || !availableTrucks.length) {
        const {acknowledged} = await loadModel
            .updateOne(loadToPost, {status: 'POSTED'});
        if (!acknowledged) {
          return res.status(500)
              .send({message: 'Database can\`t posted the load'});
        }
        return res.status(200)
            .send({message: 'Load posted successfully', driver_found: false});
      }
      // if I have available trucks
      // chech dimensions and payloads
      const suitableTrucks = availableTrucks
          .filter(
              (truck) => truckParameters[truck.type].payload >=
              loadToPost.payload &&
              truckParameters[truck.type].width >=
              loadToPost.dimensions.width &&
              truckParameters[truck.type].height >=
              loadToPost.dimensions.height &&
              truckParameters[truck.type].length >=
              loadToPost.dimensions.length);
      // if i havent suitable trucks => just post load
      if (!suitableTrucks.length) {
        const {acknowledged} = await loadModel
            .updateOne(loadToPost, {status: 'POSTED'});
        if (!acknowledged) {
          return res.status(500)
              .send({message: 'Database can\`t posted the load'});
        }
        return res.status(200)
            .send({message: 'Load posted successfully', driver_found: false});
      }

      // if i have suitable truck
      const truck = suitableTrucks[0];
      const logs = loadToPost.logs;
      logs.push({
        message: 'Load assigned to driver with id ###',
        time: Date.now(),
      });
      await loadModel
          .updateOne({_id: loadId},
              {
                status: 'ASSIGNED',
                assigned_to: truck.assigned_to.toString(),
                logs,
              });
      await truckModel.updateOne(truck, {status: 'OL'});
      res.status(200)
          .send({message: 'Load posted successfully', driver_found: true});
    } catch (error) {
      res.status(500).send({message: 'Server error zz'});
    }
  },
  getShippingInfo: async (req, res) => {
    try {
      const userId = req.userId;
      const userRole = req.role;
      const loadId = req.params['id'];
      if (userRole !== SHIPPER) {
        return res.status(400)
            .send({message: 'Permission allowed only for shipper'});
      }
      if (!loadId) {
        return res.status(400).send({message: 'Specify the load id'});
      }
      const load = await loadModel.findOne({_id: loadId, created_by: userId});
      const truck = await truckModel.findOne({assigned_to: load.assigned_to});
      res.status(200).send({load, truck});
    } catch (error) {
      res.status(500).send({message: 'Server error'});
    }
  },
};
