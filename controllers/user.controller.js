const {userModel} = require('../models/user.models');
const bcrypt = require('bcrypt');
const {changePasswordValidator} = require('../models/user.models');

const saltRounds = +process.env.BCRYPT_SALT;

module.exports = {
  getUserInfo: async (req, res) => {
    try {
      const userId = req.userId;
      const user = await userModel.findById(userId);
      if (!user) {
        return res.status(400).send({messsage: 'User not found'});
      }
      const {_id, role, email} = user;
      res.status(200).send({
        user: {_id, role, email, created_date: user.created_date},
      });
    } catch (error) {
      res.status(500).send({message: 'Server error'});
    }
  },
  deleteProfile: async (req, res) => {
    try {
      const userId = req.userId;
      const user = await userModel.findById(userId);
      if (!user) {
        return res.status(400).send({messsage: 'User not found'});
      }
      const deleteData = await userModel.deleteOne(user);
      if (!deleteData.acknowledged) {
        return res.status(500)
            .send({messsage: 'Database cant delete this user'});
      }
      res.status(200).send({messsage: 'Profile deleted successfully'});
    } catch (error) {
      res.status(500).send({message: 'Server error'});
    }
  },
  changePassword: async (req, res) => {
    try {
      const userId = req.userId;
      const user = await userModel.findById(userId);
      if (!user) {
        return res.status(400).send({messsage: 'User not found'});
      }
      const passwordsData = req.body;
      const {error} = changePasswordValidator.validate(passwordsData);
      if (error) {
        return res.status(400).send({message: error.details[0].message});
      }
      const {oldPassword, newPassword} = passwordsData;
      const isPasswordsMatch = await bcrypt.compare(oldPassword, user.password);
      if (!isPasswordsMatch) {
        return res.status(400).send({message: 'Wrong password'});
      }
      const encryptedPassword = await bcrypt.hash(newPassword, saltRounds);
      const {acknowledged} = await userModel
          .updateOne(user, {password: encryptedPassword});
      if (!acknowledged) {
        return res.status(500)
            .send({message: 'Database cant update user'});
      }
      res.status(200).send({message: 'Password changed successfully'});
    } catch (error) {
      res.status(500).send({message: 'Server error'});
    }
  },
};
