const {truckModel, addTruckValidator} = require('../models/truck.models');

const DRIVER = 'DRIVER';

module.exports = {
  addTruck: async (req, res) => {
    try {
      const userId = req.userId;
      const userRole = req.role;
      if (userRole !== DRIVER) {
        return res.status(400)
            .send({message: 'Permission allowed only for drivers'});
      }
      const reqData = req.body;
      const {error} = addTruckValidator.validate(reqData);
      if (error) {
        return res.status(400).send({message: error.details[0].message});
      };
      await truckModel.create({
        created_by: userId,
        type: reqData.type,
      });
      res.status(200).send({message: 'Truck created successfully'});
    } catch (error) {
      res.status(500).send({message: 'Server error'});
    }
  },
  getAllTrucks: async (req, res) => {
    try {
      const userId = req.userId;
      const userRole = req.role;
      if (userRole !== DRIVER) {
        return res.status(400)
            .send({message: 'Permission allowed only for drivers'});
      }
      const trucks = await truckModel.find({created_by: userId});
      res.status(200).send({trucks});
    } catch (error) {
      res.status(500).send({message: 'Server error'});
    }
  },
  getTruckById: async (req, res) => {
    try {
      const userId = req.userId;
      const userRole = req.role;
      if (userRole !== DRIVER) {
        return res.status(400)
            .send({message: 'Permission allowed only for drivers'});
      }
      const truckId = req.params['id'];
      if (!truckId) {
        return res.status(400).send({message: 'Specify truck\'s id'});
      }
      const truck = await truckModel
          .findOne({_id: truckId, created_by: userId});
      if (!truck) {
        return res.status(400).send({message: 'No truck with this ID'});
      }
      res.status(200).send({truck});
    } catch (error) {
      res.status(500).send({message: 'Server error'});
    }
  },
  updateTruckById: async (req, res) => {
    try {
      const userId = req.userId;
      const reqData = req.body;
      const userRole = req.role;
      const truckId = req.params['id'];
      const {error} = addTruckValidator.validate(reqData);
      if (error) {
        return res.status(400).send({message: error.details[0].message});
      };
      if (userRole !== DRIVER) {
        return res.status(400)
            .send({message: 'Permission allowed only for drivers'});
      }
      if (!truckId) {
        return res.status(400).send({message: 'Specify truck\'s id'});
      }
      const {acknowledged} = await truckModel
          .updateOne({_id: truckId, created_by: userId}, {type: reqData.type});
      if (!acknowledged) {
        return res.status(500).send({message: 'Database error'});
      }
      res.status(200)
          .send({message: 'Truck details changed successfully'});
    } catch (error) {
      res.status(500).send({message: 'Server error'});
    }
  },
  assignTruckBId: async (req, res) => {
    try {
      const userId = req.userId;
      const userRole = req.role;
      const truckId = req.params['id'];
      if (userRole !== DRIVER) {
        return res.status(400)
            .send({message: 'Permission allowed only for drivers'});
      }
      if (!truckId) {
        return res.status(400).send({message: 'Specify truck\'s id'});
      }

      // if driver already have assigned trucks:
      const assigned = await truckModel.findOne({assigned_to: userId});
      console.log(!!assigned);
      if (assigned) {
        return res.status(400)
            .send({message: 'You already have assigned trucks'});
      }
      const {acknowledged} = await truckModel
          .updateOne(
              {_id: truckId, created_by: userId},
              {assigned_to: userId});
      if (!acknowledged) {
        return res.status(500).send({message: 'Database error'});
      }
      res.status(200)
          .send({message: 'Truck assigned successfully'});
    } catch (error) {
      res.status(500).send({message: 'Server error'});
    }
  },
  deleteTruckById: async (req, res) => {
    try {
      const userId = req.userId;
      const userRole = req.role;
      const truckId = req.params['id'];
      if (userRole !== DRIVER) {
        return res.status(400)
            .send({message: 'Permission allowed only for drivers'});
      }
      if (!truckId) {
        return res.status(400).send({message: 'Specify truck\'s id'});
      }
      const {acknowledged} = await truckModel
          .deleteOne({_id: truckId, created_by: userId});
      if (!acknowledged) {
        return res.status(500).send({message: 'Database error'});
      }
      res.status(200)
          .send({message: 'Truck deleted successfully'});
    } catch (error) {
      res.status(500).send({message: 'Server error'});
    }
  },
};
