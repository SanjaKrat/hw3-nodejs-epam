const express = require('express');
const router = new express.Router();
const authController = require('../controllers/auth.controller');


router.post('/api/auth/register', authController.register);
router.post('/api/auth/login', authController.login);
router.post('/api/auth/forgot_password', authController.forgotPassword);

module.exports = router;
