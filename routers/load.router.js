const express = require('express');
const router = new express.Router();
const loadController = require('../controllers/load.controller');

const loadEndpoint = '/api/loads';

router.post(loadEndpoint, loadController.addLoad);
router.get(loadEndpoint, loadController.getAllLoads);
router.get(`${loadEndpoint}/active`, loadController.getActiveLoad);
router.patch(`${loadEndpoint}/active/state`, loadController.nextLoadState);
router.get(`${loadEndpoint}/:id`, loadController.getLoadById);
router.put(`${loadEndpoint}/:id`, loadController.updateLoadbyId);
router.delete(`${loadEndpoint}/:id`, loadController.deleteLoadById);
router.post(`${loadEndpoint}/:id/post`, loadController.postLoad);
router.get(`${loadEndpoint}/:id/shipping_info`, loadController.getShippingInfo);

module.exports = router;
