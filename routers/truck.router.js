const express = require('express');
const router = new express.Router();
const truckController = require('../controllers/truck.controller');

const truckEndpoint = '/api/trucks';

router.get(truckEndpoint, truckController.getAllTrucks);
router.post(truckEndpoint, truckController.addTruck);
router.get(`${truckEndpoint}/:id`, truckController.getTruckById);
router.put(`${truckEndpoint}/:id`, truckController.updateTruckById);
router.post(`${truckEndpoint}/:id/assign`, truckController.assignTruckBId);
router.delete(`${truckEndpoint}/:id`, truckController.deleteTruckById);

module.exports = router;
