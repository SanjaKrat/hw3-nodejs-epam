const express = require('express');
const router = new express.Router();
const userController = require('../controllers/user.controller');

const userEndpoint = '/api/users/me';

router.get(userEndpoint, userController.getUserInfo);
router.delete(userEndpoint, userController.deleteProfile);
router.patch(`${userEndpoint}/password`, userController.changePassword);

module.exports = router;
