const mongoose = require('mongoose');
const Joi = require('joi');

const loadStates = [
  'En route to Pick Up',
  'Arrived to Pick Up',
  'En route to delivery',
  'Arrived to delivery',
];

const Schema = mongoose.Schema;

const LoadSchema = new Schema({
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    default: null,
  },
  status: {
    type: String,
    enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
    default: 'NEW',
  },
  state: {
    type: String,
    enum: loadStates,
    default: loadStates[0],
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    width: {
      type: Number,
      required: true,
    },
    length: {
      type: Number,
      required: true,
    },
    height: {
      type: Number,
      required: true,
    },
  },
  logs: [{
    message: {type: String},
    time: {type: Date},
  }],
  created_date: {
    type: Date,
    default: Date.now(),
  },
},
{
  versionKey: false,
});

const loadModel = mongoose.model('loads', LoadSchema);

const dimensionsValidator = Joi.object().keys({
  width: Joi.number().required(),
  length: Joi.number().required(),
  height: Joi.number().required(),
});

const loadValidator = Joi.object().keys({
  name: Joi.string().required(),
  payload: Joi.number().required(),
  pickup_address: Joi.string().required(),
  delivery_address: Joi.string().required(),
  dimensions: dimensionsValidator,
});

const limitLoadValidator = Joi.number().min(10).max(50).label('limit');

module.exports = {loadModel, loadValidator, loadStates, limitLoadValidator};
