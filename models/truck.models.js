const mongoose = require('mongoose');
const Joi = require('joi');

const Schema = mongoose.Schema;

const TruckSchema = new Schema({
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    default: null,
  },
  type: {
    type: String,
    enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
    required: true,
  },
  status: {
    type: String,
    enum: ['OL', 'IS'],
    default: 'IS',
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
},
{
  versionKey: false,
});

const truckModel = mongoose.model('trucks', TruckSchema);

const truckParameters = {
  'SPRINTER': {
    width: 300,
    length: 250,
    height: 170,
    payload: 1700,
  },
  'SMALL STRAIGHT': {
    width: 500,
    length: 250,
    height: 170,
    payload: 2500,
  },
  'LARGE STRAIGHT': {
    width: 700,
    length: 350,
    height: 200,
    payload: 4000,
  },
};

const addTruckValidator = Joi.object().keys({
  type: Joi.string()
      .valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT')
      .required(),
});

module.exports = {
  truckModel,
  truckParameters,
  addTruckValidator,
};
