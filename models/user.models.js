const mongoose = require('mongoose');
const Joi = require('joi');

const Schema = mongoose.Schema;

const UserSchema = new Schema({
  role: {
    type: String,
    required: true,
    enum: ['SHIPPER', 'DRIVER'],
  },
  email: {
    type: String,
    trim: true,
    lowercase: true,
    required: true,
  },
  password: {
    type: String,
    trim: true,
    required: true,
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
},
{
  versionKey: false,
});

const userModel = mongoose.model('User', UserSchema);

const userRegisterValidator = Joi.object().keys({
  role: Joi.string().valid('SHIPPER', 'DRIVER').required(),
  email: Joi.string().email().required(),
  password: Joi.string().required(),
});

const userLoginValidator = Joi.object().keys({
  email: Joi.string().email().required(),
  password: Joi.string().required(),
});

const changePasswordValidator = Joi.object().keys({
  oldPassword: Joi.string().required(),
  newPassword: Joi.string().required(),
});

const userEmailValidator = Joi.object().keys({
  email: Joi.string().email().required(),
});

module.exports = {
  userModel,
  userRegisterValidator,
  userLoginValidator,
  changePasswordValidator,
  userEmailValidator,
};
