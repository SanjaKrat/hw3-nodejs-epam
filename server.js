require('dotenv').config();
require('./database/database').connect();
const express = require('express');
const logger = require('morgan');
const fs = require('fs');
const cors = require('cors');
const auth = require('./middleware/auth.middleware');
const authRoute = require('./routers/auth.router');
const userRoute = require('./routers/user.router');
const truckRouter = require('./routers/truck.router');
const loadRouter = require('./routers/load.router');


const app = express();
const PORT = process.env.PORT || 8080;

app.use(express.json());
app.use(cors());
app.use(logger('combined', {
  stream: fs.createWriteStream('./access.log', {flags: 'a'}),
}));
app.use(logger('dev'));

app.use(authRoute);
app.use(auth, userRoute);
app.use(auth, truckRouter);
app.use(auth, loadRouter);

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
