# HW3 NodeJS Epam


UBER like service for freight trucks. This service should help regular people to deliver their stuff and help drivers to find loads and earn some money.

A shipper can post his loads and system find available trucks for this. 
A driver can add his transport and do some delivery - the system automatically finds loads for you!


## Getting started

For run this app just clone it in your computer and start app using:
```
git clone https://gitlab.com/SanjaKrat/hw3-nodejs-epam.git
npm install
npm start
```
By default, the application starts on port 8080. 

For developing purpose you can use: 
```
npm run dev
```

## Prerequisites
Attention: it requires a pre-installed NodeJS and Nodemon package. <br />
### Node JS installation ###
Go to [NodeJS.org](https://nodejs.org/) and follow instructions. 

### Nodemon installation ###
For global installation (the recommended way according to official docs):
```
npm install -g nodemon
```
or as a development dependency:
```
npm install --save-dev nodemon
```

Read more about Nodemon in [Nodemon Documentation](https://github.com/remy/nodemon#nodemon).
